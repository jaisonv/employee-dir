import XCTest
import APIClient
import DependencyInjection
@testable import EmployeeList

final class EmployeeListTests: XCTestCase {

    @MainActor
    func test_valid_employees_list_will_load() throws {
        let viewModel = EmployeeListViewModel()

        InjectedValues[\.apiClient] = .mockValid

        let expectation = self.expectation(description: "Send request")
        viewModel.fetchEmployees()

        let subscriber = viewModel.$state
            .receive(on: RunLoop.main)
            .removeDuplicates()
            .sink { _ in
                guard viewModel.state == .loaded else { return }
                expectation.fulfill()
            }

        wait(for: [expectation], timeout: 1)
        XCTAssertTrue(!viewModel.employees.isEmpty)
        XCTAssertTrue(viewModel.state == .loaded)
        subscriber.cancel()
    }

    @MainActor
    func test_invalid_employees_list_will_fail() throws {
        let viewModel = EmployeeListViewModel()

        InjectedValues[\.apiClient] = .mockMalformed
        let expectation = self.expectation(description: "Send request")
        viewModel.fetchEmployees()

        let subscriber = viewModel.$state
            .receive(on: RunLoop.main)
            .removeDuplicates()
            .sink { _ in
                guard viewModel.state == .error else { return }
                expectation.fulfill()
            }

        wait(for: [expectation], timeout: 1)
        XCTAssertTrue(viewModel.employees.isEmpty)
        XCTAssertTrue(viewModel.state == .error)
        subscriber.cancel()
    }

    @MainActor
    func test_empty_employees_list_will_load_empty() throws {
        let viewModel = EmployeeListViewModel()

        InjectedValues[\.apiClient] = .mockEmpty
        let expectation = self.expectation(description: "Send request")
        viewModel.fetchEmployees()

        let subscriber = viewModel.$state
            .receive(on: RunLoop.main)
            .removeDuplicates()
            .sink { _ in
                guard viewModel.state == .loaded else { return }
                expectation.fulfill()
            }

        wait(for: [expectation], timeout: 1)
        XCTAssertTrue(viewModel.employees.isEmpty)
        XCTAssertTrue(viewModel.state == .loaded)
        subscriber.cancel()
    }
}
