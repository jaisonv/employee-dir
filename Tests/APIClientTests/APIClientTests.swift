import XCTest
import APIClient

class APIClientTests: XCTestCase {

    func test_json_response_is_valid() async throws {
        let employees = try await APIClient.mockValid.allEmployees()
        XCTAssertNotNil(employees)
    }

    func test_json_response_is_invalid() async throws {
        let employees = try? await APIClient.mockMalformed.allEmployees()
        XCTAssertNil(employees)
    }

    func test_json_response_is_empty() async throws {
        let employees = try await APIClient.mockEmpty.allEmployees()
        XCTAssertEqual(employees.count, 0)
    }

    func test_download_valid_image() async throws {
        guard let imageURL = Bundle.module.url(forResource: "block", withExtension: "jpg")?.absoluteString else {
            XCTAssert(false, "Test Image not available, or url is no longer valid.")
            return
        }

        let image = try await APIClient.mockValid.downloadImage(imageURL, URLCache())
        XCTAssertNotNil(image)
    }

    func test_image_will_load_from_cache() async throws {

        guard let imageURL = Bundle.module.url(forResource: "block", withExtension: "jpg") else {
            XCTAssert(false, "Test Image not available, or url is no longer valid.")
            return
        }

        let cache = URLCache.shared
        let request = URLRequest(url: imageURL)

        XCTAssertNil(APIClient.loadCachedData(cache: cache, request: request))

        let _ = try await APIClient.mockValid.downloadImage(imageURL.absoluteString, cache)

        XCTAssertNotNil(APIClient.loadCachedData(cache: cache, request: request))
    }
}
