import Foundation
import Combine
import APIClient
import DependencyInjection

@MainActor
public final class EmployeeListViewModel {
    enum State {
        case idle
        case loading
        case loaded
        case error
    }

    @Injected(\.apiClient) private var apiClient
    private(set) var employees = [EmployeeItemViewModel]()
    @Published private(set) var state = State.idle 

    func fetchEmployees() {
        state = .loading
        Task {
            do {
                let employeesResponse = try await apiClient.allEmployees()
                employees = employeesResponse.map { EmployeeItemViewModel(employeeResponse: $0) }
                state = .loaded
            } catch {
                state = .error
            }
        }
    }
}

