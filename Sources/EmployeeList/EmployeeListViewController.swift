import UIKit
import Combine

final class EmployeesListViewController: UITableViewController {

    typealias ViewModelType = EmployeeListViewModel

    private var viewModel: EmployeeListViewModel
    private var cancellables: Set<AnyCancellable> = []

    init(viewModel: EmployeeListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Employee Directory"

        tableView.backgroundColor = .white
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(pullToRefresh), for: .allEvents)
        tableView.register(EmployeeTableViewCell.self, forCellReuseIdentifier: "cell")

        tableView.allowsSelection = false
        bindViewModel()

        viewModel.fetchEmployees()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.employees.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "cell",
            for: indexPath
        ) as? EmployeeTableViewCell else {
            fatalError("Cell could not be created")
        }

        cell.setupView(viewModel: viewModel.employees[indexPath.row])

        return cell
    }

    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if refreshControl?.isRefreshing == true {
            viewModel.fetchEmployees()
        }
    }

    // MARK: private

    private func bindViewModel() {
        viewModel.$state
            .receive(on: RunLoop.main)
            .removeDuplicates()
            .sink { [weak self] _ in
            self?.handleRefresh()
        }
        .store(in: &cancellables)
    }

    @objc private func pullToRefresh() {
        viewModel.fetchEmployees()
    }

    private func handleRefresh() {
        removeEmptyStateLabel()

        switch viewModel.state {
        case .idle:
            break
        case .loading:
            tableView.refreshControl?.beginRefreshing()
            break
        case .loaded:
            tableView.refreshControl?.endRefreshing()
            if viewModel.employees.isEmpty {
                presentErrorLabel(message: "No employees to display")
            }
        case .error:
            tableView.refreshControl?.endRefreshing()
            presentErrorLabel(message: "There was an error!")
        }

        tableView.reloadData()
    }

    private func presentErrorLabel(message: String) {
        let messageLabel = UILabel()
        messageLabel.text = message
        messageLabel.textAlignment = .center
        tableView.backgroundView = messageLabel
    }

    private func removeEmptyStateLabel() {
        tableView.backgroundView = nil
    }
}

#if canImport(SwiftUI) && DEBUG
import SwiftUI

struct EmployeesListViewController_Previews: PreviewProvider {

    static var previews: some View {
        ContainerView()
    }

    struct ContainerView: UIViewControllerRepresentable {
        typealias UIViewControllerType = UIViewController

        func makeUIViewController(context: Context) -> UIViewController {
            return EmployeesListViewController(viewModel: EmployeeListViewModel())
        }

        func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
        }
    }
}
#endif
