import UIKit
import SnapKit

final class EmployeeTableViewCell: UITableViewCell {

    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 18)
        return label
    }()

    let phoneLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14)
        return label
    }()

    let emailLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15)
        return label
    }()

    let typeLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = .boldSystemFont(ofSize: 15)
        return label
    }()

    let teamLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 15)
        return label
    }()

    let bioLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 13)
        return label
    }()

    let photoImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.tintColor = .gray
        imageView.image = UIImage(systemName: "person.crop.square")
        return imageView
    }()

    func setupView(viewModel: EmployeeItemViewModel) {
        setupConstraints()
        
        nameLabel.text = viewModel.fullName
        phoneLabel.text = viewModel.phoneNumber
        emailLabel.text = viewModel.email
        teamLabel.text = viewModel.team
        bioLabel.text = viewModel.bio
        typeLabel.text = viewModel.employeeType.short

        switch viewModel.employeeType {
        case .fullTime:
            typeLabel.textColor = .blue
        case .partTime:
            typeLabel.textColor = .green
        case .contractor:
            typeLabel.textColor = .orange
        case .unknown:
            typeLabel.textColor = .gray
        }

        Task {
            if let imageData = await viewModel.loadImage() {
                photoImageView.image = UIImage(data: imageData)
            }
        }
    }

    private func setupConstraints() {

        let phoneIconImageView: UIImageView = {
            let imageView = UIImageView(frame: .zero)
            imageView.tintColor = .gray
            imageView.image = UIImage(systemName: "phone")
            imageView.contentMode = .scaleAspectFit
            return imageView
        }()

        let emailIconImageView: UIImageView = {
            let imageView = UIImageView(frame: .zero)
            imageView.tintColor = .gray
            imageView.image = UIImage(systemName: "envelope")
            imageView.contentMode = .scaleAspectFit
            return imageView
        }()

        let corpIconImageView: UIImageView = {
            let imageView = UIImageView(frame: .zero)
            imageView.tintColor = .gray
            imageView.image = UIImage(systemName: "case")
            imageView.contentMode = .scaleAspectFit
            return imageView
        }()

        contentView.addSubview(phoneIconImageView)
        contentView.addSubview(emailIconImageView)
        contentView.addSubview(corpIconImageView)
        contentView.addSubview(photoImageView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(typeLabel)
        contentView.addSubview(phoneLabel)
        contentView.addSubview(emailLabel)
        contentView.addSubview(teamLabel)
        contentView.addSubview(bioLabel)

        photoImageView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.width.height.equalTo(90)
            make.left.equalToSuperview().offset(20)
        }

        nameLabel.snp.makeConstraints { make in
            make.left.equalTo(photoImageView.snp.right).offset(5)
            make.top.equalTo(photoImageView.snp.top)
        }

        typeLabel.snp.makeConstraints { make in
            make.left.equalTo(nameLabel.snp.right).offset(5)
            make.right.equalToSuperview().inset(20)
            make.top.equalTo(photoImageView.snp.top)
        }

        phoneIconImageView.snp.makeConstraints { make in
            make.width.height.equalTo(20)
            make.left.equalTo(photoImageView.snp.right).offset(5)
            make.top.equalTo(nameLabel.snp.bottom).offset(6)
        }

        phoneLabel.snp.makeConstraints { make in
            make.left.equalTo(phoneIconImageView.snp.right).offset(5)
            make.right.equalToSuperview().inset(20)
            make.top.equalTo(nameLabel.snp.bottom).offset(6)
        }

        emailIconImageView.snp.makeConstraints { make in
            make.width.height.equalTo(20)
            make.left.equalTo(photoImageView.snp.right).offset(5)
            make.top.equalTo(phoneLabel.snp.bottom).offset(3)
        }

        emailLabel.snp.makeConstraints { make in
            make.left.equalTo(emailIconImageView.snp.right).offset(5)
            make.right.equalToSuperview().inset(20)
            make.top.equalTo(phoneLabel.snp.bottom).offset(3)
        }

        corpIconImageView.snp.makeConstraints { make in
            make.width.height.equalTo(20)
            make.left.equalTo(photoImageView.snp.right).offset(5)
            make.top.equalTo(emailLabel.snp.bottom).offset(3)
        }

        teamLabel.snp.makeConstraints { make in
            make.left.equalTo(corpIconImageView.snp.right).offset(5)
            make.right.equalToSuperview().inset(20)
            make.top.equalTo(emailLabel.snp.bottom).offset(3)
        }

        bioLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().inset(20)
            make.top.equalTo(teamLabel.snp.bottom).offset(5)
            make.bottom.equalToSuperview().inset(10)
        }
    }
}

#if canImport(SwiftUI) && DEBUG
import SwiftUI
struct EmployeeTableViewCell_Previews: PreviewProvider {
    
    static var previews: some View {
        CellPreviewContainer()
            .frame(height: 130)
    }

    struct CellPreviewContainer: UIViewRepresentable {
        typealias UIViewType = UITableViewCell

        func makeUIView(context: Context) -> UIViewType {
            let tableViewCell = EmployeeTableViewCell()
            tableViewCell.setupView(
                viewModel: .mock
            )
            return tableViewCell
        }

        func updateUIView(_ uiView: UIViewType, context: Context) {

        }
    }
}
#endif
