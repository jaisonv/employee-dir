import Foundation
import APIClient
import DependencyInjection

@MainActor
final class EmployeeItemViewModel {

    enum EmployeeType: String {
        case fullTime = "FULL_TIME"
        case partTime = "PART_TIME"
        case contractor = "CONTRACTOR"
        case unknown

        var short: String {
            switch self {
            case .fullTime:
                return "FT"
            case .partTime:
                return "PT"
            case .contractor:
                return "C"
            case .unknown:
                return "U"
            }
        }
    }

    @Injected(\.apiClient) private var apiClient

    var fullName: String
    var phoneNumber: String?
    var email: String
    var team: String
    var employeeType: EmployeeType
    var bio: String?
    var photoURL: String?

    init(employeeResponse: Employee.Result) {
        self.fullName = employeeResponse.fullName
        self.phoneNumber = employeeResponse.phoneNumber
        self.email = employeeResponse.emailAddress
        self.team = employeeResponse.team
        self.employeeType = EmployeeType(rawValue: employeeResponse.employeeType) ?? .unknown
        self.bio = employeeResponse.biography
        self.photoURL = employeeResponse.photoUrlSmall
    }

    init(fullName: String, phoneNumber: String, email: String, team: String, employeeType: String, bio: String) {
        self.fullName = fullName
        self.phoneNumber = phoneNumber
        self.email = email
        self.team = team
        self.employeeType = EmployeeType(rawValue: employeeType) ?? .unknown
        self.bio = bio
    }

    func loadImage() async -> Data? {
        if let photoURL = photoURL {
            return try? await apiClient.downloadImage(photoURL, URLCache.shared)
        }
        return nil
    }
}

#if DEBUG
extension EmployeeItemViewModel {
    static let mock = EmployeeItemViewModel(
        fullName: "Jaison Vieira",
        phoneNumber: "987-654-3210",
        email: "jaison@abc.com",
        team: "Engineering",
        employeeType: "FULL_TIME",
        bio: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    )
}
#endif
