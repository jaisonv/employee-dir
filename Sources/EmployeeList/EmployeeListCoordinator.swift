import UIKit

public final class EmployeeListCoordinator {
    var navigationController: UINavigationController

    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    @MainActor
    public func start() {
        let employeeListViewModel = EmployeeListViewModel()
        let employeeListViewController = EmployeesListViewController(viewModel: employeeListViewModel)
        navigationController.pushViewController(employeeListViewController, animated: false)
    }
}
