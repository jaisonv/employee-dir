import Foundation
import DependencyInjection

public struct Employee: Decodable {
    var employees: [Result]

    public struct Result: Decodable {
        public var uuid: String
        public var fullName: String
        public var phoneNumber: String?
        public var emailAddress: String
        public var biography: String?
        public var photoUrlSmall: String?
        public var photoUrlLarge: String?
        public var team: String
        public var employeeType: String
    }
}

public struct APIClient {
    public var allEmployees: () async throws -> [Employee.Result]
    public var downloadImage: (String, URLCache) async throws -> Data
}

public extension APIClient {
    static let live = Self(
        allEmployees: {
            guard let url = URL(string: baseURL + "/employees.json") else {
                fatalError("Invalid URL pattern.")
            }
            let (data, _) = try await URLSession.shared.data(from: url)
            return try jsonDecoder.decode(Employee.self, from: data).employees
        },
        downloadImage: { imageURL, cache in
            guard let url = URL(string: imageURL) else { return Data() }
            let request = URLRequest(url: url)

            if let cachedImageData = loadCachedData(cache: cache, request: request) {
                return cachedImageData
            }

            let (data, response) = try await URLSession.shared.data(for: request)
            let cachedResponse = CachedURLResponse(response: response, data: data)
            cache.storeCachedResponse(cachedResponse, for: request)
            return data
        }
    )

    static func loadCachedData(cache: URLCache, request: URLRequest) -> Data? {
        return cache.cachedResponse(for: request)?.data
    }
}

private let baseURL: String = {
    return "https://s3.amazonaws.com/sq-mobile-interview"
}()

private let jsonDecoder: JSONDecoder = {
    let decoder = JSONDecoder()
    decoder.keyDecodingStrategy = .convertFromSnakeCase
    return decoder
}()

// MARK: - Dependency
private struct ApiClientKey: InjectionKey {
    static var currentValue = APIClient.live
}

public extension InjectedValues {
    var apiClient: APIClient {
        get { Self[ApiClientKey.self] }
        set { Self[ApiClientKey.self] = newValue }
    }
}


// MARK: - Mocks
#if DEBUG
public extension APIClient {
    static let mockValid = Self(
        allEmployees: {
            return try jsonDecoder.decode(Employee.self, from: validJson).employees
        },
        downloadImage: mockDownLoadImage
    )
}

public extension APIClient {
    static let mockMalformed = Self(
        allEmployees: {
            return try jsonDecoder.decode(Employee.self, from: malformedJson).employees
        },
        downloadImage: mockDownLoadImage
    )
}

public extension APIClient {
    static let mockEmpty = Self(
        allEmployees: {
            return try jsonDecoder.decode(Employee.self, from: emptyJson).employees
        },
        downloadImage: mockDownLoadImage
    )
}

private var mockDownLoadImage: (String, URLCache) async throws -> Data = { imageURL, cache in

    guard let url = URL(string: imageURL) else {
        fatalError("Invalid URL pattern.")
    }
    let request = URLRequest(url: url)

    if let cachedImageData = APIClient.loadCachedData(cache: cache, request: request) {
        return cachedImageData
    }

    let imageData = try Data(contentsOf: url)
    let cachedResponse = CachedURLResponse(response: URLResponse.init(url: url, mimeType: nil, expectedContentLength: 1, textEncodingName: nil), data: imageData)
    cache.storeCachedResponse(cachedResponse, for: request)

    return imageData
}
#endif
