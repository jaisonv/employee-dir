# EmployeeDirectory

## Build tools & versions used
The project was built using `Xcode` along with `Swift Package Manager`. It's required `Xcode` version `14.3` and up.

## Steps to run the app
Open the `Application` directory and run the file `Application.xcodeproj`.

Select the Scheme `Application` and run from there. This Scheme is also the one used to run all the tests. Hit CMD + U.

## What areas of the app did you focus on?
My main focus is on modularity. Each feature belongs to its own module. 

Secondly I focused on simplicity. The `APIClient` is simple, I didn't see the need for building a whole network layer for this project and giving the responsibility for the model to request its data and handle the response seemed like a good solution for this project. I also didn't use any library for image caching, since I could achieve the goals using native `URLCache`.

## What was the reason for your focus? What problems were you trying to solve?
That helps with separation and building the app faster. Also by using previews when building the UI, it's less error prone and harder to crash the previews. 

## How long did you spend on this project?
Around 8 hours.

## Did you make any trade-offs for this project? What would you have done differently with more time?
No trade-offs. I would add more tests for the `EmployeeList` module and some visual styling like masks for phone numbers and adjust the UI to nicely hide elements when they are nil.

## What do you think is the weakest part of your project?
The coordinator is too simple. It works for this project but in a bigger project it would need to be more complex and handle children making sure they are released from the memory when dismissed.

## Did you copy any code or dependencies? Please make sure to attribute them here!
I used the code for DependencyInjection from [avanderlee.com](https://www.avanderlee.com/swift/dependency-injection/). This implementation uses some cool Swift features without the need of thirdy party libraries.

Used [Snapkit](https://github.com/SnapKit/SnapKit) for setting the constraints in the views.

## Is there any other information you’d like us to know?
Tasks done:
- [x] Employee List
- [x] Pull to refresh feature
- [x] Error and empty list handled
- [x] Empty list handled
- [x] Photos are cached
- [x] Minimal test coverage

An overview of the app architecture.

![Alt text](images/arch_overview.png)
