// swift-tools-version: 5.8

import PackageDescription

let package = Package(
    name: "EmployeeDirectory",
    platforms: [.iOS(.v15),],
    products: [
        .library(
            name: "EmployeeList",
            targets: ["EmployeeList"]
        ),
        .library(
            name: "APIClient",
            targets: ["APIClient"]
        ),
        .library(
            name: "DependencyInjection",
            targets: ["DependencyInjection"]
        )
    ],
    dependencies: [
        .package(
            url: "https://github.com/SnapKit/SnapKit.git",
                .upToNextMajor(from: "5.0.1")
        )
    ],
    targets: [
        .target(
            name: "EmployeeList",
            dependencies: [
                "APIClient",
                "DependencyInjection",
                .product(
                    name: "SnapKit",
                    package: "SnapKit"
                )
            ]
        ),
        .testTarget(
            name: "EmployeeListTests",
            dependencies: ["EmployeeList"]
        ),
        .target(
            name: "APIClient",
            dependencies: ["DependencyInjection"]
        ),
        .testTarget(
            name: "APIClientTests",
            dependencies: ["APIClient"],
            resources: [.process("Assets")]
        ),
        .target(name: "DependencyInjection")
    ]
)
